Statistics Package Toolbox

Purpose
-------

This module is a virtual module, which allows to install 
several statistics modules in one single atomsInstall() 
statement. 

This virtual module gathers:

 * Distfun : a collection of distribution functions (e.g. normal, 
   exponential, etc...)
 * Stixbox : statistical functions (e.g. polyfit, qqplot, etc...)
 * Scidoe : design of experiments (e.g. lhs design, regress, etc...)
 * Lowdisc :  low discrepancy sequences (e.g. Halton, Sobol, etc...)

Together, these toolbox provides a consistent set of tested and 
documented functions for statistics.
Hence, this avoids the user to search for functions in several 
toolboxes. 

Moreover, this module allows to potentially solve double-dependencies 
problems, that is, a situation where a module A depends on a module B 
and vice-versa. 
Although this situation should be avoided in general, it may 
happen anyway.


Dependencies
------------

 * This module depends on Scilab (>= v5.4).
 * This module depends on the distfun module
 * This module depends on the stixbox module
 * This module depends on the scidoe module
 * This module depends on the lowdisc module

Authors
-------

 * Copyright (C) 2013 - Michael Baudin

Licence
------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

